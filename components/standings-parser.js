import { parseRussianFloat } from './float-parser'

export default function(html) {
  const parser = new DOMParser()
  const htmlDoc = parser.parseFromString(html, 'text/html')
  const standings = []
  const lines = htmlDoc.getElementsByClassName('stand')[0].getElementsByTagName('tr')
  const header = lines[0].children

  for (let line_id = 1; line_id < lines.length; ++line_id) {
    const tr = lines[line_id]
    if (tr.getElementsByTagName('th').length > 0) {
      break
    }
    const columns = tr.children
    const info = []
    const pointsPerProblem = []
    const stats = []
    let currentColumnId = 2
    let currentColumn
    while (((currentColumn = columns[currentColumnId]), !currentColumn.classList.contains('res'))) {
      info.push(currentColumn.innerText)
      ++currentColumnId
    }
    while (((currentColumn = columns[currentColumnId]), currentColumn.classList.contains('res'))) {
      let points = 0
      const ACMPoints = currentColumn.getElementsByClassName('dig')
      if (ACMPoints.length > 0) {
        if (ACMPoints[0].innerText === '+') {
          points = 100
        } else {
          points = -1 - (parseInt(ACMPoints[0].nextSibling.textContent) || 0)
        }
      } else {
        points = parseRussianFloat(currentColumn.innerText)
      }
      pointsPerProblem.push({
        points: points,
        html: currentColumn.innerHTML,
      })
      ++currentColumnId
    }
    while (((currentColumn = columns[currentColumnId]), currentColumnId < columns.length)) {
      stats.push(currentColumn.innerText)
      ++currentColumnId
    }

    standings.push({
      id: columns[1].children[0].getAttribute('href').slice(9),
      place: columns[0].innerText,
      name: columns[1].innerText,
      info,
      pointsPerProblem,
      stats,
      total: parseRussianFloat(stats[0]),
    })
  }

  return {
    title: htmlDoc.getElementsByTagName('h2')[0].innerText,
    columnNames: Array.from(header).map(th => th.innerText),
    standings,
  }
}
