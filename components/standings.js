import { PureComponent } from 'react'
import FlipMove from 'react-flip-move'

import { parseRussianFloat } from './float-parser'
import { binarySearch } from './binary-search'
import parseStandingsHTML from './standings-parser'

class TD extends PureComponent {
  state = {
    className: '',
  }

  constructor(props) {
    super(props)
    this._timer = null
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState === this.state) {
      clearTimeout(this._timer)
      this.setState({ className: '' })
      let className = ''
      const newComparisonValue = this.props.comparisonValue
      const prevComparisonValue = prevProps.comparisonValue
      if (
        newComparisonValue > prevComparisonValue ||
        (newComparisonValue > 0 && isNaN(prevComparisonValue))
      ) {
        className = 'went-up'
      } else {
        className = 'went-down'
      }
      this._timer = setTimeout(() => {
        this.setState({ className: className })
        this._timer = setTimeout(() => {
          this.setState({ className: '' })
        }, 4000)
      }, 10)
    }
  }

  render() {
    const { className, html, children, ...props } = this.props
    if (html) {
      props.dangerouslySetInnerHTML = { __html: html }
    }
    return (
      <td {...props} className={`${className} ${this.state.className}`}>
        {children}
      </td>
    )
  }
}

class StandingsLine extends PureComponent {
  render() {
    const infoColumns = this.props.info.map((value, index) => (
      <TD key={index} className="pc">
        {value}
      </TD>
    ))
    const problems = this.props.pointsPerProblem.map(({ points, html }, index) => (
      <TD
        key={index}
        className={`res ${points === 100 ? 'solved' : ''}`}
        comparisonValue={points}
        html={html}
      />
    ))
    const statsColumns = this.props.stats.map((value, index) => (
      <TD key={index} className="pc" comparisonValue={index === 0 ? this.props.total : 0}>
        {value}
      </TD>
    ))
    return (
      <tr className={this.props.className}>
        <td className="pr">{this.props.place}</td>
        <td className="pl">
          <a href="">{this.props.name}</a>
        </td>
        {infoColumns}
        {problems}
        {statsColumns}
      </tr>
    )
  }
}

export default class extends PureComponent {
  state = {
    title: '',
    infoColumns: [],
    problems: [],
    statsColumns: [],
    standings: [],
    ranks: {
      updates: {},
      groups: {},
    },
    currentSolutionId: null,
  }

  _suggestGroupForLine(
    { oldRanks, oldStandings, newStandings, updates },
    prevLineGroup,
    index,
    line
  ) {
    if (index === 0 && updates[line.id] === 0) {
      return oldRanks.groups[line.id]
    }
    const excludedGroups = []
    if (index > 0) {
      if (line.total === newStandings[index - 1].total) {
        return prevLineGroup
      }
      excludedGroups.push(prevLineGroup)
    }
    if (updates[line.id] != 0 && oldStandings.length) {
      const sameTotalStandingIndex = binarySearch(oldStandings, line, (a, b) => a.total - b.total)
      if (sameTotalStandingIndex >= 0) {
        const nextGroup = oldRanks.groups[oldStandings[sameTotalStandingIndex].id]
        if (excludedGroups.indexOf(nextGroup) < 0) {
          return nextGroup
        }
      } else {
        const nextLowerTotalStandingIndex = -sameTotalStandingIndex
        if (nextLowerTotalStandingIndex < oldStandings.length) {
          excludedGroups.push(oldRanks.groups[oldStandings[nextLowerTotalStandingIndex].id])
        }
      }
    }

    if (line.id in oldRanks.groups && excludedGroups.indexOf(oldRanks.groups[line.id]) < 0) {
      return oldRanks.groups[line.id]
    }
    const availableGroups = [0, 1, 2].filter(group => excludedGroups.indexOf(group) < 0)
    return availableGroups[Math.floor(availableGroups.length * Math.random())]
  }

  _computeRanks(oldStandings, newStandings, oldRanks) {
    const prevRanks = {}
    oldStandings.forEach((line, index) => (prevRanks[line.id] = index))
    const updates = {}
    newStandings.forEach((line, index) => {
      updates[line.id] = prevRanks[line.id] - index
    })

    const context = {
      oldRanks,
      oldStandings,
      newStandings,
      updates,
    }
    const groups = {}
    let prevGroup = null
    newStandings.forEach((line, index) => {
      prevGroup = groups[line.id] = this._suggestGroupForLine(context, prevGroup, index, line)
    })

    return {
      updates,
      groups,
    }
  }

  _fetchStandings = () => {
    let standingsUrl = `${this.props.dotsUrl}/standings?cid=${this.props.contestId}`
    if (this.state.currentSolutionId) {
      standingsUrl += `&last_solution=${this.state.currentSolutionId}`
    }
    const xrequest = new XMLHttpRequest()
    xrequest.open('GET', standingsUrl, true)
    if (this.props.charset) {
      xrequest.overrideMimeType(`text/plain; charset=${this.props.charset}`)
    }
    xrequest.onload = () => {
      const newState = parseStandingsHTML(xrequest.responseText)
      newState.ranks = this._computeRanks(
        this.state.standings,
        newState.standings,
        this.state.ranks
      )
      if (this.state.currentSolutionId) {
        newState.currentSolutionId = this.state.currentSolutionId + this.props.solutionIdIncrement
      }
      this.setState(newState)
    }
    xrequest.send('')
  }

  _scheduleFetchStandings = () => {
    this._fetchStandings()
    this._timer = setTimeout(this._scheduleFetchStandings, 2000)
  }

  componentWillMount() {
    if (typeof window !== 'undefined') {
      if (this.props.firstSolutionId) {
        this.state.currentSolutionId = this.props.firstSolutionId
      }
      this._scheduleFetchStandings()
    }
  }

  componentWillUnmount() {
    clearTimeout(this._timer)
  }

  render() {
    if (!this.state.standings.length) {
      return <div style={this.props.style}>Загрузка...</div>
    }
    const standings = this.state.standings.map((line, index) => {
      let className = ''
      if (line.id in this.state.ranks.groups) {
        className += `taskgroup${this.state.ranks.groups[line.id]}`
        const rankUpdate = this.state.ranks.updates[line.id]
        if (rankUpdate > 0) {
          className += ' went-up'
        } else if (rankUpdate < 0) {
          className += ' went-down'
        }
      }
      return <StandingsLine key={line.id} {...line} className={className} />
    })
    return (
      <div style={this.props.style}>
        <h2>{this.state.title}</h2>
        <style>{`
          @keyframes went-up {
            30% { background-color: rgba(169, 208, 124, 0.5); }
            60% { background-color: rgba(169, 208, 124, 0.5); }
          }

          .went-up {
            animation: went-up 5s;
          }

          @keyframes went-down {
            30% { background-color: rgba(255, 179, 0, 0.4); }
          }

          .went-down {
            animation: went-down 5s;
          }

          table.stand tr.taskgroup0 {
            background-color: rgba(53, 150, 197, 0.20);
            transition: background-color 5s ease;
          }

          table.stand tr.taskgroup1 {
            background-color: rgba(176, 213, 232, 0.15);
            transition: background-color 5s ease;
          }

          table.stand tr.taskgroup2 {
            background-color: rgba(106, 185, 224, 0.18);
            transition: background-color 5s ease;
          }

          /* Workaround to prevent flickering of CSS transitions:
           * http://www.bluepiccadilly.com/2012/01/prevent-flickering-css-transitions-and-transforms-webkit 
           */
          tr {
            -webkit-backface-visibility: hidden;
          }
        `}</style>
        <table className="stand otbor_stand acm_stand" style={{ width: '100%' }} cellSpacing="0">
          <thead>
            <tr>
              {this.state.columnNames.map(columnName => <th key={columnName}>{columnName}</th>)}
            </tr>
          </thead>
          <FlipMove typeName="tbody" duration={1500} staggerDurationBy={30}>
            {standings}
          </FlipMove>
        </table>
      </div>
    )
  }
}
