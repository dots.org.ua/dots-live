export function parseRussianFloat(str) {
  return parseFloat(str.replace(' ', '').replace(',', '.'))
}
