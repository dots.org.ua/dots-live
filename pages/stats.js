import Head from 'next/head'

import papaparse from 'papaparse'
import querystring from 'querystring'

const LANG_NAME_BY_ID = {
  2: 'C (C89)',
  18: 'C (C11)',

  3: 'C++ (C++03)',
  19: 'C++ (C++11)',
  20: 'C++ (C++14)',
  30: 'C++ (C++17)',

  4: 'Pascal',
  39: 'Delphi',

  14: 'C# mono',
  29: 'VB.NET',
  35: 'C# .NET',

  13: 'Java 7',
  17: 'Java 8',
  24: 'Scala',
  26: 'Kotlin',

  16: 'Go',
  21: 'Haskell',
  22: 'Nim',
  23: 'Rust',
  33: 'OCaml',
  34: 'Swift',

  11: 'Python 2',
  12: 'Python 3',
  28: 'Python Machine Learning',
  15: 'Ruby',
  25: 'PHP',
  27: 'Bash',
  31: 'JavaScript (Node.JS)',
  32: 'Jaguar',
}

const LANG_ID_BY_NAME = {}
Object.entries(LANG_NAME_BY_ID).forEach(([id, name]) => {
  LANG_ID_BY_NAME[name] = id
})

const LANG_GROUPS = {
  C: ['C (C89)', 'C (C11)'],
  'C++': ['C++ (C++03)', 'C++ (C++11)', 'C++ (C++14)', 'C++ (C++17)'],
  'C#': ['C# mono', 'C# .NET'],
  Pascal: ['Pascal', 'Delphi'],
  Java: ['Java 7', 'Java 8'],
  Python: ['Python 2', 'Python 3', 'Python Machine Learning'],
}
Object.keys(LANG_ID_BY_NAME).forEach(name => {
  if (!Object.values(LANG_GROUPS).some(groupLangs => groupLangs.indexOf(name) >= 0)) {
    LANG_GROUPS[name] = [name]
  }
})

export default class extends React.Component {
  state = {
    fields: [],
  }

  SQL(db, token, sql) {
    const form = new FormData()
    form.append('db', db)
    form.append('table', 'any')
    form.append('token', token)
    form.append('single_table', 'TRUE')
    form.append('export_type', 'table')
    form.append('sql_query', sql)
    form.append('what', 'csv')
    form.append('csv_separator', ',')
    form.append('csv_enclosed', '"')
    form.append('csv_escaped', '"')
    form.append('csv_terminated', 'AUTO')
    form.append('csv_null', 'NULL')
    form.append('csv_columns', 'something')
    form.append('csv_data', '')
    form.append('asfile', 'sendit')
    return fetch('/myadminphp/export.php', {
      credentials: 'include',
      method: 'POST',
      body: form,
    })
      .then(resp => resp.text())
      .then(text => {
        const parsedData = papaparse.parse(text, { header: true, skipEmptyLines: true })
        return { data: parsedData.data, fields: parsedData.meta.fields }
      })
  }

  componentWillMount() {
    if (typeof window !== 'undefined') {
      const query = querystring.parse(window.location.search.slice(1))
      this.state = {
        db: query.db,
        tablesPrefix: query.tables_prefix,
        contestId: query.contest_id,
        solutionIdRange: [query.solution_id__gte, query.solution_id__lte],
      }
    }
  }

  _fetchStats = ({ token, whereClause }) => {
    const { db, tablesPrefix } = this.state
    const solutionsTable = `${tablesPrefix}solutions`
    this.SQL(
      db,
      token,
      `SELECT COUNT(DISTINCT user_id) as total_users_count, COUNT(*) as total_solutions_count, SUM(case when test_result=0 then 1 else 0 end) as OK_solutions_count, SUM(case when test_result=2 then 1 else 0 end) as CE_solutions_count FROM \`${solutionsTable}\` ${
        whereClause ? `WHERE ${whereClause}` : ''
      }`
    ).then(({ data }) => {
      this.setState({ solutionsCount: data[0] })
    })
    const langGroupsClause = `(CASE ${Object.entries(LANG_GROUPS)
      .map(
        ([groupName, groupLangs]) =>
          `WHEN ${groupLangs
            .map(lang => `${solutionsTable}.lang_id = ${LANG_ID_BY_NAME[lang]}`)
            .join(' OR ')} THEN '${groupName}'`
      )
      .join(' ')} ELSE 'Other' END)`
    this.SQL(
      db,
      token,
      `SELECT ${langGroupsClause} as lang, COUNT(DISTINCT user_id) as users_count, COUNT(*) as solutions_count, SUM(case when test_result=0 then 1 else 0 end) as OK_solutions_count, SUM(case when test_result=2 then 1 else 0 end) as CE_solutions_count FROM \`${solutionsTable}\` ${
        whereClause ? `WHERE ${whereClause}` : ''
      } GROUP BY ${langGroupsClause} ORDER BY users_count DESC`
    ).then(({ data }) => {
      this.setState({ solutionsCountByLanguage: data })
    })
  }

  componentDidMount() {
    fetch('/myadminphp/index.php', { credentials: 'include' })
      .then(response => response.text())
      .then(text => {
        let whereClauseItems = []
        if (this.state.contestId) {
          whereClauseItems.push(`(\`contest_id\` = ${this.state.contestId})`)
        }
        if (this.state.solutionIdRange[0]) {
          whereClauseItems.push(`(\`solution_id\` >= ${this.state.solutionIdRange[0]})`)
        }
        if (this.state.solutionIdRange[1]) {
          whereClauseItems.push(`(\`solution_id\` <= ${this.state.solutionIdRange[1]})`)
        }
        this._fetchStats({
          token: text.match(/'token=([^']*)/)[1],
          whereClause: whereClauseItems.join(' AND '),
        })
      })
  }

  render() {
    const { db, tablesPrefix } = this.state
    if (!db || !tablesPrefix) {
      return (
        <React.Fragment>
          <Head>
            <title>Статистика ДОТС</title>
          </Head>
          <form>
            <div>
              <label>
                Имя Базы Данных: <input name="db" defaultValue="qbit_ddots" />
              </label>
            </div>
            <div>
              <label>
                Префикс таблицы: <input name="tables_prefix" defaultValue="olympkh_" />
              </label>
            </div>
            <div>
              <label>
                ID турнира (опционально): <input name="contest_id" />
              </label>
            </div>
            <div>
              <label>
                Минимальный solution ID (опционально): <input name="solution_id__gte" />
              </label>
            </div>
            <div>
              <label>
                Максимальный solution ID (опционально): <input name="solution_id__lte" />
              </label>
            </div>
            <div>
              <input type="submit" />
            </div>
          </form>
        </React.Fragment>
      )
    }

    return (
      <div className="container my-5">
        <Head>
          <title>Статистика ДОТС</title>
        </Head>
        <link
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
          crossorigin="anonymous"
        />
        <h1 className="my-5">{`Статистика по БД "${db}" (${tablesPrefix}*)`}</h1>
        <h2>{`Статистика по языкам программирования`}</h2>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">{`Язык программирования`}</th>
              <th scope="col">{`Количество уникальных авторов`}</th>
              <th scope="col">{`Количество решений`}</th>
              <th scope="col">{`Количество правильных решений`}</th>
              <th scope="col">{`Количество решений c ошибкой компиляции`}</th>
            </tr>
          </thead>
          <tbody>
            {(this.state.solutionsCountByLanguage || []).map(
              ({ lang, users_count, solutions_count, OK_solutions_count, CE_solutions_count }) => (
                <tr key={lang}>
                  <td>{lang}</td>
                  <td>{users_count}</td>
                  <td>{solutions_count}</td>
                  <td>{`${OK_solutions_count} (${(
                    OK_solutions_count /
                    solutions_count *
                    100
                  ).toFixed(0)}%)`}</td>
                  <td>{`${CE_solutions_count} (${(
                    CE_solutions_count /
                    solutions_count *
                    100
                  ).toFixed(0)}%)`}</td>
                </tr>
              )
            )}
          </tbody>
          <tfoot>
            <tr>
              <th />
              {!this.state.solutionsCount ? (
                <th colSpan="3">{`Загрузка...`}</th>
              ) : (
                <React.Fragment>
                  <th>{this.state.solutionsCount.total_users_count}</th>
                  <th>{this.state.solutionsCount.total_solutions_count}</th>
                  <th>{`${this.state.solutionsCount.OK_solutions_count} (${(
                    this.state.solutionsCount.OK_solutions_count /
                    this.state.solutionsCount.total_solutions_count *
                    100
                  ).toFixed(0)}%)`}</th>
                  <th>{`${this.state.solutionsCount.CE_solutions_count} (${(
                    this.state.solutionsCount.CE_solutions_count /
                    this.state.solutionsCount.total_solutions_count *
                    100
                  ).toFixed(0)}%)`}</th>
                </React.Fragment>
              )}
            </tr>
          </tfoot>
        </table>
      </div>
    )
  }
}
