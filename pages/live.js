import Head from 'next/head'

import querystring from 'querystring'

import Standings from '../components/standings'

export default class extends React.PureComponent {
  state = {}

  componentWillMount() {
    if (typeof window !== 'undefined') {
      const query = querystring.parse(window.location.search.slice(1))
      this.state = {
        dotsUrl: query.dots_url || '', // "" means same domain. You may want to be explicit: "http://olympkh.dots.org.ua"
        charset: query.charset,
        title: query.title,
        contestIds: typeof query.contest_id === 'string' ? [query.contest_id] : query.contest_id,
        firstSolutionId: parseInt(query.first_solution_id),
        solutionIdIncrement: parseInt(query.solution_id_increment),
        overflowHidden: query.overflow_hidden,
      }
    }
  }

  render() {
    const { dotsUrl, charset, title, contestIds } = this.state
    if (typeof title === 'undefined' || typeof contestIds === 'undefined') {
      return (
        <React.Fragment>
          <style>{`
            #dots-live-form .field {
              width: 300px;
              display: inline-block;
              text-align: left;
            }
            #dots-live-form .field input {
              width: 100%;
            }
          `}</style>
          <form
            id="dots-live-form"
            style={{ margin: '200px auto', width: 700, textAlign: 'right' }}
          >
            <div>
              <label>
                DOTS URL-префикс:{' '}
                <div className="field">
                  <input type="text" name="dots_url" />
                </div>
              </label>
            </div>
            <div>
              <label>
                Кодировка:{' '}
                <div className="field">
                  <select name="charset">
                    <option value="utf-8">utf-8</option>
                    <option value="windows-1251">windows-1251</option>
                  </select>
                </div>
              </label>
            </div>
            <div>
              <label>
                Название турнира:{' '}
                <div className="field">
                  <input type="text" name="title" />
                </div>
              </label>
            </div>
            <div>
              <label>
                ID турнира:{' '}
                <div className="field">
                  <input type="text" name="contest_id" />
                </div>
              </label>
            </div>
            <div>
              <label>
                ID первого решения (опционально):{' '}
                <div className="field">
                  <input type="text" name="first_solution_id" />
                </div>
              </label>
            </div>
            <div>
              <label>
                Шаг увеличения ID решений (опционально):{' '}
                <div className="field">
                  <input type="text" name="solution_id_increment" />
                </div>
              </label>
            </div>
            <div>
              <input type="submit" value="Старт" />
            </div>
          </form>
        </React.Fragment>
      )
    }
    return (
      <div id="content" style={{ margin: 0, padding: 0, height: 10000, minHeight: 10000 }}>
        <Head>
          <title>{title} - live</title>
          <link rel="stylesheet" href={`${dotsUrl}/screen.css`} type="text/css" media="all" />
          {this.state.overflowHidden && <style>{`html {overflow: hidden; }`}</style>}
        </Head>

        <h1 style={{ fontSize: 32, padding: 30 }}>{title}</h1>
        {contestIds.map((contestId, index) => (
          <Standings
            key={contestId}
            dotsUrl={dotsUrl}
            charset={charset}
            contestId={contestId}
            firstSolutionId={this.state.firstSolutionId}
            solutionIdIncrement={this.state.solutionIdIncrement}
            style={{
              width: 100 / contestIds.length - 1.51 + '%',
              paddingLeft: index === 0 ? '0.5%' : '1.5%',
              float: 'left',
            }}
          />
        ))}
      </div>
    )
  }
}
