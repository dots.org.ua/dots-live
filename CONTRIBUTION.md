Для форматирования кода DOTS Live используется Prettier:

```
$ ./node_modules/.bin/prettier --no-semi --print-width 100 --single-quote --trailing-comma es5 --write '{pages,components}/**/*.js'
```
